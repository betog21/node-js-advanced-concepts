# Node JS Advanced Concepts
Describes how to improve Node.js performance with the creation of new node instances using [p2m](https://www.npmjs.com/package/pm2) and [Worker Threads](https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API/Using_web_workers).

This file is divided into two main parts: **Clustering** and **Worker threads**.
Each section contains a brief explanation, some recomended videos and the commands to run the examples.

For this project a server with two endpoints is created. One endpoint is localhost:3000, which simulates a time consuming task, and the second endpoint is localhost:3000/fast, which responds almost immediately.

Each technique solves the time consuming task.

# Before starting
Install the dependencies running:
```
npm install
```

# Clustering
This section uses the [clustering techique](https://nodejs.org/api/cluster.html#cluster):

> Clusters of Node.js processes can be used to run multiple instances of Node.js that can distribute workloads among their application threads. When process isolation is not needed, use the worker_threads module instead, which allows running multiple application threads within a single Node.js instance.

## Files for this section
- clustering.js
- clusteringPM2.js

In the **clustering.js** file you'll find how to manage the clustering technique in an initial way,  completely depending on the programer.

The **clusteringPM2.js** section is an example about how you can run it with the pm2 library.
## Recomended videos
In order to understand better how Node runs on diferents threads, it's recomended to watch the next videos:

[Here's Why Node JS is NOT Single Threaded](https://www.youtube.com/watch?v=JwSEqEalg7E) (08:21 duration)

[CLUSTER MULTI THREADED NODEJS -- Run threads in parallel to speed up long processes (CORRECTED)](https://www.youtube.com/watch?v=hXb95jm-kk0)(15:28 duration)

[How to scale NodeJs applications using the cluster module.](https://www.youtube.com/watch?v=9RLeLngtQ3A)(13:36 duration)

[Increase Node JS Performance With Thread Pool Management 📊 | OPTIMIZING NODE JS](https://www.youtube.com/watch?v=LC5FC3FdzAE)(12:28 duration)

## Commands

## Clustering
For the first example you can run the clustering.js file just like a normal node file:
```
node clustering.js
```

## PM2

### Install pm2 globally
Install the library [pm2](https://www.npmjs.com/package/pm2) globally.

```
npm install pm2 -g
```

### Run clusteringPM2.js with pm2
```
pm2 start clusteringPM2.js -i 0
```
### View a quick summary of the health of all your running instances
```
pm2 list
```
### View a details of a specific instance
```
pm2 show clusteringPM2
```
### Monit all the running instances
```
pm2 monit 
```

### Stop all the running instances
```
pm2 delete clusteringPM2
```

# Worker threads
This section uses the Worker [threads techique](https://nodejs.org/docs/latest-v16.x/api/worker_threads.html):

> The node:worker_threads module enables the use of threads that execute JavaScript in parallel.

## Recomended lecture
[Using Web Workers](https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API/Using_web_workers)


## Files for this section
- worker.js
- workerThreads.js

In the **workerThreads.js** file you'll find the node serverver that handles the requests and in the rooth endpoint you'll find the creation of a new worker.

In the **worker.js** file you'll find the worker used in the workerThreads.js file.

## Recomended videos
In order to understand better how worker threads works, it's recomended to watch the next videos:

[Managing Multiple Threads In Node JS 🧵 | OPTIMIZING NODE JS](https://www.youtube.com/watch?v=W0go0ve1XE0)(9:31 duration)

[Speed Up Your Node App Using Worker Threads!](https://www.youtube.com/watch?v=Qg68f6Ir1SA)(6:32 duration)

## Commands
### Run the example with workerThreads
```
node workerThreads.js
```

## ⚠️ Note:
It's recommended to don't use Clustering and Worker threads at the same time.
